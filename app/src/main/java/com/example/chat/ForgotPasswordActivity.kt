package com.example.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_log_in.*

class ForgotPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        init()
    }

    private fun init() {
        repair_email_forgot_pass_send_button.setOnClickListener {
            repairPassword()
        }
    }

    private fun repairPassword() {
        val auth = FirebaseAuth.getInstance()
        val emailAddress = repair_email_forgot_pass.text.toString()

        if (repair_email_forgot_pass.text.toString().isEmpty()) {
            repair_email_forgot_pass.error = "Enter Email"
            repair_email_forgot_pass.requestFocus()
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(repair_email_forgot_pass.text.toString()).matches()) {
            repair_email_forgot_pass.error = "Incorrect"
            repair_email_forgot_pass.requestFocus()
            return
        }
        auth.sendPasswordResetEmail(emailAddress)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(baseContext, "Check your email", Toast.LENGTH_LONG).show()
                    repair_email_forgot_pass.setText("")
                }else{
                    Toast.makeText(baseContext, "incorrect email", Toast.LENGTH_LONG).show()
                }
            }
    }
}


