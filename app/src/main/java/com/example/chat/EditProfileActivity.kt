package com.example.chat

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.view.isVisible
import com.example.chat.models.User
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.save_new_password_button
import java.util.*

class EditProfileActivity : AppCompatActivity() {

    private lateinit var database: DatabaseReference
    lateinit var storage: FirebaseStorage
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        database = FirebaseDatabase.getInstance().reference
        storage = FirebaseStorage.getInstance()
        auth = FirebaseAuth.getInstance()
        supportActionBar?.title = "Edit Profile"

        readInformation()
        init()
    }

    private fun init() {
        editImageButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/+"
            startActivityForResult(intent, 0)
        }
        save_new_password_button.setOnClickListener {
            startActivity(Intent(this, ChangePasswordActivity::class.java))
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {

            selectedPhotoUri = data.data

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)

            edit_profile_image.setImageBitmap(bitmap)
            editImageButton.alpha = 0f
        }
    }

    var selectedPhotoUri: Uri? = null

    private fun changeUserProfile() {
        if (selectedPhotoUri == null) {
            val uid = FirebaseAuth.getInstance().uid ?: ""
            database.child("/users/$uid").child("username")
                .setValue(username_edit_profile.text.toString())
            database.child("/users/$uid").child("password")
                .setValue(password_edit_profile.text.toString())
            database.child("/users/$uid").child("email")
                .setValue(email_edit_profile.text.toString())

            Toast.makeText(baseContext, "Profile Updated", Toast.LENGTH_SHORT).show()
        } else {
            val filename = UUID.randomUUID().toString()
            val ref = FirebaseStorage.getInstance().getReference("images/$filename")

            ref.putFile(selectedPhotoUri!!)
                .addOnSuccessListener {

                    ref.downloadUrl.addOnSuccessListener {
                        saveUserToFirebaseDatabase(it.toString())
                        Toast.makeText(baseContext, "Profile Updated", Toast.LENGTH_SHORT).show()
                    }
                }
                .addOnFailureListener {
                    Toast.makeText(baseContext, "Unsuccessfully Update", Toast.LENGTH_SHORT).show()
                }
        }
    }

    private fun saveUserToFirebaseDatabase(profileImageUrl: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
        val user = User(
            uid,
            email_edit_profile.text.toString(),
            password_edit_profile.text.toString(),
            username_edit_profile.text.toString(),
            profileImageUrl
        )
        ref.setValue(user)
            .addOnSuccessListener {

            }
    }

    private fun readInformation() {
        val uid = FirebaseAuth.getInstance().uid ?: ""

        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("/users/$uid")
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue(User::class.java)

                val username = value!!.username
                username_edit_profile.setText(username)
                val password = value.password
                password_edit_profile.setText(password)
                val email = value!!.email
                email_edit_profile.setText(email)
                Picasso.get().load(value.profileImageUrl).into(edit_profile_image)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w("real_time", "Failed to read value.", error.toException())
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.menu_save_profile -> {
                changeUserProfile()
                val intent = Intent(this, ProfileActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.save_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

}

