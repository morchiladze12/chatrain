package com.example.chat.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class User(
    val uid: String,
    val email: String,
    val password: String,
    val username: String,
    val profileImageUrl: String
) : Parcelable {
    constructor() : this("", "", "", "", "")
}