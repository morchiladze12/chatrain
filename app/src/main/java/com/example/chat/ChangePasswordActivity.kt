package com.example.chat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.save_new_password_button
import kotlinx.android.synthetic.main.activity_log_in.*

class ChangePasswordActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance().reference
        supportActionBar?.title = "Password"

        init()
    }

    private fun init() {
        save_new_password_button.setOnClickListener {
            changePassword()
        }
    }

    private fun changePassword() {
        if (current_password_change.text.isNotEmpty() &&
            new_password_change.text.isNotEmpty()
        ) {

            val user = auth.currentUser
            if (user != null && user.email != null) {
                val credential = EmailAuthProvider
                    .getCredential(user.email!!, current_password_change.text.toString())
                user?.reauthenticate(credential)
                    ?.addOnCompleteListener {
                        if (it.isSuccessful) {
                            user?.updatePassword(new_password_change.text.toString())
                                ?.addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        val uid = FirebaseAuth.getInstance().uid ?: ""
                                        database.child("/users/$uid").child("password")
                                            .setValue(new_password_change.text.toString())
                                        Toast.makeText(
                                            this,
                                            "Password Changed successfully",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        startActivity(Intent(this, EditProfileActivity::class.java))
                                        finish()
                                    }else{
                                        new_password_change.error = "Weak Password"
                                        new_password_change.requestFocus()
                                    }
                                }
                        } else {
                            current_password_change.error = "Incorrect"
                            current_password_change.requestFocus()
                        }
                    }
            } else {
                startActivity(Intent(this, LogInActivity::class.java))
                finish()
            }

        } else {
            Toast.makeText(this, "Fill all fields", Toast.LENGTH_SHORT).show()
        }
    }
}